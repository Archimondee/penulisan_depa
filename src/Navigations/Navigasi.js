import {createStackNavigator} from 'react-navigation';
import {
    HomeScreen,
    RecipeScreen,
    ListScreen
}from '../Components';

export default Main = createStackNavigator({
    Home: {screen: HomeScreen},
    Recipes: {screen: RecipeScreen},
    List: {screen: ListScreen}
},{
    initialRouteName: 'Home',
    headerMode:'none'
})