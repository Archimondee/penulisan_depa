import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Body,
  Right,
  Left, 
  Title,
  Button
} from 'native-base';

import {View, ScrollView, Image, Alert, TouchableOpacity, Text, ImageBackground, StatusBar} from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import db from '../../../const/db.js';
import { SearchBar } from 'react-native-elements';

export default class HomeScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            data:db,
            entertain:[],
            isSearch: false
        },
        this.arrayholder = []
    }
    componentWillMount(){
        console.log(this.state.data[1].name);
        console.log(this.state.data[1].image);
        //this._renderEntertaining();
    }
    _entertainingYonanas(){
        let a = this.state.data;
        let tain = []
        a.map((items)=>{
            if(items.category === 1){
                tain.push(
                    items
                )
            }
        });
        return tain
    }
    _bananaYonana(){
        let b = this.state.data;
        let bana = [];
        b.map((items)=>{
            if(items.category === 2){
                bana.push(
                    items
                )
            }
        })
        return bana;
    }
    _noBanana(){
        let c = this.state.data;
        let no = [];
        c.map((items)=>{
            if(items.category === 3){
                no.push(
                    items
                )
            }
        })
        return no;
    }
    _searchFilter(text){
        const arrayData = this.state.data;
        const namaItem = [];
        const dataBaru = [];
        arrayData.map((items)=>{
            namaItem.push(
                items.name
            )
        })
        const newData = namaItem.filter(function(items){
            const itemData = items.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > - 1
        })
        
        console.log(newData);
        if (text === ""){
            this.setState({
                isSearch: false,
                data: db
            })
        }else{
            this.setState({
                isSearch: true
            })
            newData.map((i, j)=>{
                arrayData.map((l, j)=>{
                    if(i==l.name){
                        dataBaru.push(
                            l
                        )
                    }
                })
            })
            this.setState({
                data: dataBaru
            })
        }
        //console.log(namaItem);
        //console.log(item);
    }
    
  render () {
      const adaEntertaining = this._entertainingYonanas();
      const adaBanana = this._bananaYonana();
      const adaNo = this._noBanana();
    return (
      <Container>
      <ImageBackground source={require('../../../assets/bg.png')} style={{height:"100%", width:"100%", flex:1}} resizeMode='cover'>
            <Header style={{backgroundColor:'purple', height:70, paddingTop:StatusBar.currentHeight}}>
          <Left>
            <Ionicons name="md-menu" size={40} color="white"/>
          </Left>
          <Body>
            <Text style={{color:'white', fontWeight:'bold'}}>Yonanas Recipes</Text>
          </Body>
          <Right />
        </Header>
        <View style={{marginTop:5, paddingLeft:5, marginRight:5, flexDirection:'row'}}>
                <View style={{width:"80%"}}>
                    <SearchBar
                        lightTheme
                        searchIcon={{size:24}}
                        placeholder="Search here"
                        onChangeText={(item)=>this._searchFilter(item)}
                    />
                </View>
                
            <Right style={{marginRight:5}}>
                <Button style={{backgroundColor:'purple', width:60, height:50, alignItems:'center', alignContent:'center', justifyContent:'center'}}>
                    <Text style={{textAlign:'center', color:'white'}}>Search</Text>
                </Button>
            </Right>
            
        </View>
        <Content padder>
          <Card>
            <CardItem header bordered style={{backgroundColor:'purple'}}>
            <View>
                <Text style={{color: 'white'}} >Entertaining Yonanas</Text>
            </View>
            <Right style={{marginRight:-60}} >
                <Text style={{textDecorationLine:'underline', color: 'white'}}
                    onPress={()=>this.props.navigation.navigate('List',{data:adaEntertaining, nama:"Entertaining Yonanas"})}
                >View All</Text>
            </Right>
            </CardItem>
            <CardItem style={{}}>
                <ScrollView horizontal={true}>
                    {
                        this._entertainingYonanas().map((items)=>{
                            const a = 0;
                        return(
                                <View key={items.id} style={{height:200, width:200, marginBottom:50}}>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("Recipes",{recipe: items.recipes, name:items.name, gambar:items.image})}>
                                        <View>
                                            <Image source={items.image} 
                                                style={{width:190, height:190}}
                                            />
                                        </View>
                                        <View style={{flexDirection:'row', marginTop:10}} >
                                            <Text style={{width:0, flexGrow:1, flex:1}}>
                                                {items.name}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                    
                                </View>
                        )
                    })}
                </ScrollView>
            </CardItem>
          </Card>

          <Card>
            <CardItem header bordered style={{backgroundColor:'purple'}}>
            <View>
                <Text style={{color: 'white'}} >Banana Yonanas</Text>
            </View>
            <Right style={{marginRight:-60}}>
                <Text style={{textDecorationLine:'underline', color: 'white'}}
                    onPress={()=>this.props.navigation.navigate('List',{data:adaBanana, nama: "Banana Yonanas"})}
                >View All</Text>
            </Right>
            </CardItem>
            <CardItem style={{}}>
                <ScrollView horizontal={true}>
                    {
                        this._bananaYonana().map((items)=>{
                            const a = 0;
                        return(
                                <View key={items.id} style={{height:200, width:200, marginBottom:50}}>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("Recipes",{recipe: items.recipes, name:items.name, gambar:items.image})}>
                                        <View>
                                            <Image source={items.image} 
                                                style={{width:190, height:190}}
                                            />
                                        </View>
                                        <View style={{flexDirection:'row', marginTop:10}} >
                                            <Text style={{width:0, flexGrow:1, flex:1}}>
                                                {items.name}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                    
                                </View>
                             
                        )
                    })}
                </ScrollView>
            </CardItem>
          </Card>

          <Card>
            <CardItem header bordered style={{backgroundColor:'purple'}}>
            <View>
                <Text style={{color: 'white'}} >No Banana Yonanas Recipes</Text>
            </View>
            <Right style={{marginRight:-40}}>
                <Text style={{textDecorationLine:'underline', color: 'white'}}
                    onPress={()=>this.props.navigation.navigate('List',{data:adaNo, nama: "No Banana Yonanas Recipes"})}
                >View All</Text>
            </Right>
            </CardItem>
            <CardItem style={{}}>
                <ScrollView horizontal={true}>
                    {
                        this._noBanana().map((items)=>{
                            const a = 0;
                        return(
                                <View key={items.id} style={{height:200, width:200, marginBottom:50}}>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("Recipes",{recipe: items.recipes, name:items.name, gambar:items.image})}>
                                        <View>
                                            <Image source={items.image} 
                                                style={{width:190, height:190}}
                                            />
                                        </View>
                                        <View style={{flexDirection:'row', marginTop:10}} >
                                            <Text style={{width:0, flexGrow:1, flex:1}}>
                                                {items.name}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                    
                                </View>
                        )
                    })}
                </ScrollView>
            </CardItem>
          </Card>
                                
        </Content>
      </ImageBackground>
        
      </Container>
    );
  }
}
