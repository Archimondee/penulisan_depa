import React, { Component } from 'react';
import {  View, Text, Image, TouchableOpacity, AsyncStorage, ImageBackground, StatusBar } from 'react-native';
import {
  Header,
  Icon,
  Content,
  Container,
  Card,
  CardItem,
  Left,
  Right,
  Body
} from 'native-base';
import {Ionicons} from '@expo/vector-icons';
import data from '../../../const/db';

export default class RecipeScreen extends Component {
  constructor(props){
    super(props);
      this.state={
        checked:true,
        name: "md-star-outline",

        dataRecipes : this.props.navigation.getParam('recipe'),
        foodName : this.props.navigation.getParam('name'),
        foodImage : this.props.navigation.getParam('gambar'),

      }
  }

  componentWillMount(){
    // /console.log(this.state.dataRecipes);
    //console.log(AsyncStorage.getItem('Favorites'));
  }
  render() {
    const data = this.state.dataRecipes;
    const nama = this.state.foodName;
    const gambar = this.state.foodImage;
    //console.log(data.ingredient);
    return (
      <Container>
      <ImageBackground source={require('../../../assets/bg.png')} style={{height:"100%", width:"100%", flex:1}} resizeMode='cover'>
        <Header style={{backgroundColor:'purple', height:70, paddingTop:StatusBar.currentHeight}}>
          <Left>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
              <Ionicons name="ios-arrow-back" size={40} color="white"/>
            </TouchableOpacity>
          </Left>
          <Body>
            <Text style={{color:'white', fontWeight:'bold', fontSize:15}}>{nama}</Text>
          </Body>
        </Header>
        <Content padder>
          <Card>
            <CardItem>
              <Image source={gambar} style={{height:330, width:'100%'}}/>
            </CardItem>
            <CardItem style={{justifyContent:'center', alignContent:'center'}}>
              <Text style={{fontSize:17}}>{nama}</Text>
            </CardItem>
            <CardItem style={{justifyContent:'space-between', borderBottomWidth:2, borderStyle:'dotted', marginLeft:10, marginRight:10}}>
              <View style={{flexDirection:'column'}}>
                <Image source={require('../../../assets/time_1.png')} style={{height:50, width:50}}/>
                <Text style={{textAlign:'center', paddingTop:2}}>PREP</Text>
                <Text style={{textAlign:'center', paddingTop:2}}>{data.prep} min.</Text>
              </View>
              <View style={{flexDirection:'column'}}>
                <Image source={require('../../../assets/time_1.png')} style={{height:50, width:50}}/>
                <Text style={{textAlign:'center', paddingTop:2}}>COOK</Text>
                <Text style={{textAlign:'center', paddingTop:2}}>{data.cook} min.</Text>
              </View>
              <View style={{flexDirection:'column'}}>
                <Image source={require('../../../assets/time2.png')} style={{height:50, width:50}}/>
                <Text style={{textAlign:'center', paddingTop:2}}>TOTAL</Text>
                <Text style={{textAlign:'center', paddingTop:2}}>{data.prep + data.cook} min.</Text>
              </View>
              <View style={{flexDirection:'column'}}>
                <Image source={require('../../../assets/ppl.png')} style={{height:50, width:50}}/>
                <Text style={{textAlign:'center', paddingTop:2}}>SERVES</Text>
                <Text style={{textAlign:'center', paddingTop:2}}>{data.serves}</Text>
              </View>
            </CardItem>
            
            <CardItem>
              <Text style={{fontWeight:'bold', fontSize:16}}>Ingredients :</Text>
            </CardItem>
            <CardItem style={{marginLeft:10, flexDirection:'column'}}> 
                {
                  data.ingredient.map((items, i)=>{
                    return (
                      <View key={i} style={{flexDirection:'row'}}>
                        <Text>{i+1}.</Text>
                        <Text style={{paddingLeft: 5, flex:1}}>{items}</Text>
                      </View>
                    )
                  })
                }

            </CardItem>

            <CardItem>
              <Text style={{fontWeight:'bold', fontSize:16}}>Directions :</Text>
            </CardItem>
            <CardItem style={{marginLeft:10, flexDirection:'column'}}> 
                {
                  data.directions.map((items, i)=>{
                    return (
                      <View key={i} style={{flexDirection:'row'}}>
                        <Text>{i+1}.</Text>
                        <Text style={{paddingLeft: 5, flex:1}}>{items}</Text>
                      </View>
                    )
                  })
                }
            </CardItem>
            <CardItem>
            
            </CardItem>
          </Card>
        </Content>
      </ImageBackground>
        
      </Container>
    );
  }
}
