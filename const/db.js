export default data = [
    {
        "id":1,
        "name":"Affogato Yonanas",
        "category":1,
        "image": require('../src/img/entertaining_yonanas/affogato_yonanas.jpg'),
        "recipes":
            {
                "prep" : 15,
                "cook" : 15,
                "serves" : 3,
                "ingredient": [
                    "2 Frozen over-ripe bananas.",
                    "1/2 teaspoon ground cinnamon.",
                    "1 shot of espresso or strong coffe.",
                    "OPTIONAL: chocolate sauce."
                ],
                "directions":[
                    "Slightly thaw your frozen fruit prior to putting through your yonanas maker.",
                    "Insert 1/2 frozen banana",
                    "Add 1/2 teaspoon ground cinnamon",
                    "Insert remaining frozen bananas",
                    "Scoop into glass or bowl",
                    "Optional: Drizzle your favorite chocolate sauce on top of Yonanas",
                    "Pour a shot of espresso or coffee over top",
                ]
            }
    },
    {
        "id": 2,
        "name": "Berry Chocolate Zin Yonanas",
        "category": 1,
        "image": require('../src/img/entertaining_yonanas/berry_chocolate_zin_yonanas.jpg'),
        "recipes":
            {
                "prep" : 15,
                "cook" : 10,
                "serves" : 2,
                "ingredient": [
                    "2 frozen over-ripe bananas",
                    "1 oz. dark chocolate",
                    "1 cup frozen mixed berries",
                    "2 frozen cubes of red wine",
                ],
                "directions":[
                    "Make the Red Wine Cubes in advance: Freeze leftover red wine in ice cube trays - any red wine will be delicious!",
                    "Slightly thaw your frozen fruit prior to putting through your yonanas maker.",
                    "Insert one frozen banana",
                    "Add 1 oz. dark chocolate",
                   "Add 1/2 cup frozen mixed berries",
                    "Add 2 cubes of frozen red wine",
                    "Add 1/2 cup mixed berries",
                    "Insert second frozen banana",
                ]
            }
        
    },{
        "id": 3,
        "name": "5 Layer Yonanas Cake",
        "category": 1,
        "image": require('../src/img/entertaining_yonanas/layer_yonanas_cake.jpg'),
        "recipes":
            {
                "prep" : 15,
                "cook" : 20,
                "serves" : 1,
                "ingredient": [
                    "Strawberry Yonanas (double this recipe)",
                    "Chocolate Yonanas (double this recipe)",
                    "Blueberry Yonanas (double this recipe)",
                    "Cookies & Cream Yonanas (double this recipe)",
                    "Cherry Yonanas (double this recipe)",
                    "Optional toppings: fresh fruit, herbs, edible flowers, chocolate sauce or any of your favorite toppings",
                    "NOTE: We used a 7 springform cake pan that is 3 deep."
                ],
                "directions":[
                    "Make a double batch of Strawberry Yonanas recipe",
                    "Scoop Strawberry Yonanas into a round cake pan",
                    "With an offset spatula spread Strawberry Yonanas evenly into cake pan",
                    "Repeat Steps 1-3 with each of the remaining Yonanas flavors in this order: Chocolate Yonanas, Blueberry Yonanas, Cookies & Cream Yonanas and Cherry Yonanas. Carefully smooth each layer on top of the last in the pan.",
                    "Cover and freeze for a minimum of four hours",
                    "To remove cake from mold, run warm water around the sides of the cake mold, set a plate on top of cake container and flip over. Gently remove mold.",
                    "Decorate with your favorite toppings.",
                    "Allow cake to thaw a few minutes before slicing to serve.",
                ]
            }
        
    },{
        "id":4,
        "name": "Acai Yonanas",
        "category" : 2,
        "image" : require('../src/img/banana_yonana_recipes/acai_yonanas.jpg'),
        "recipes":
            {
                "prep" : 15,
                "cook" : 20,
                "serves" : 1,
                "ingredient": [
                    "Strawberry Yonanas (double this recipe)",
                    "Chocolate Yonanas (double this recipe)",
                    "Blueberry Yonanas (double this recipe)",
                    "Cookies & Cream Yonanas (double this recipe)",
                    "Cherry Yonanas (double this recipe)",
                    "Optional toppings: fresh fruit, herbs, edible flowers, chocolate sauce or any of your favorite toppings",
                    "NOTE: We used a 7 springform cake pan that is 3 deep."
                ],
                "directions":[
                    "Make a double batch of Strawberry Yonanas recipe",
                    "Scoop Strawberry Yonanas into a round cake pan",
                    "With an offset spatula spread Strawberry Yonanas evenly into cake pan",
                    "Repeat Steps 1-3 with each of the remaining Yonanas flavors in this order: Chocolate Yonanas, Blueberry Yonanas, Cookies & Cream Yonanas and Cherry Yonanas. Carefully smooth each layer on top of the last in the pan.",
                    "Cover and freeze for a minimum of four hours",
                    "To remove cake from mold, run warm water around the sides of the cake mold, set a plate on top of cake container and flip over. Gently remove mold.",
                    "Decorate with your favorite toppings.",
                    "Allow cake to thaw a few minutes before slicing to serve.",
                ]
            
            }
        
    },{
        "id":5,
        "name": "Apple Mapple Walnut Yonanas",
        "category" : 2,
        "image" : require('../src/img/banana_yonana_recipes/apple_mapple_walnut_yonanas.jpg'),
        "recipes":
            {
                "prep" : 15,
                "cook" : 20,
                "serves" : 1,
                "ingredient": [
                    "Strawberry Yonanas (double this recipe)",
                    "Chocolate Yonanas (double this recipe)",
                    "Blueberry Yonanas (double this recipe)",
                    "Cookies & Cream Yonanas (double this recipe)",
                    "Cherry Yonanas (double this recipe)",
                    "Optional toppings: fresh fruit, herbs, edible flowers, chocolate sauce or any of your favorite toppings",
                    "NOTE: We used a 7 springform cake pan that is 3 deep."
                ],
                "directions":[
                    "Make a double batch of Strawberry Yonanas recipe",
                    "Scoop Strawberry Yonanas into a round cake pan",
                    "With an offset spatula spread Strawberry Yonanas evenly into cake pan",
                    "Repeat Steps 1-3 with each of the remaining Yonanas flavors in this order: Chocolate Yonanas, Blueberry Yonanas, Cookies & Cream Yonanas and Cherry Yonanas. Carefully smooth each layer on top of the last in the pan.",
                    "Cover and freeze for a minimum of four hours",
                    "To remove cake from mold, run warm water around the sides of the cake mold, set a plate on top of cake container and flip over. Gently remove mold.",
                    "Decorate with your favorite toppings.",
                    "Allow cake to thaw a few minutes before slicing to serve.",
                ]
            }
        
    },{
        "id":6,
        "name": "Apple Pie Yonanas",
        "category" : 2,
        "image" : require('../src/img/banana_yonana_recipes/apple_pie_yonanas.jpg'),
        "recipes":
            {
                "prep" : 15,
                "cook" : 20,
                "serves" : 1,
                "ingredient": [
                    "Strawberry Yonanas (double this recipe)",
                    "Chocolate Yonanas (double this recipe)",
                    "Blueberry Yonanas (double this recipe)",
                    "Cookies & Cream Yonanas (double this recipe)",
                    "Cherry Yonanas (double this recipe)",
                    "Optional toppings: fresh fruit, herbs, edible flowers, chocolate sauce or any of your favorite toppings",
                    "NOTE: We used a 7 springform cake pan that is 3 deep."
                ],
                "directions":[
                    "Make a double batch of Strawberry Yonanas recipe",
                    "Scoop Strawberry Yonanas into a round cake pan",
                    "With an offset spatula spread Strawberry Yonanas evenly into cake pan",
                    "Repeat Steps 1-3 with each of the remaining Yonanas flavors in this order: Chocolate Yonanas, Blueberry Yonanas, Cookies & Cream Yonanas and Cherry Yonanas. Carefully smooth each layer on top of the last in the pan.",
                    "Cover and freeze for a minimum of four hours",
                    "To remove cake from mold, run warm water around the sides of the cake mold, set a plate on top of cake container and flip over. Gently remove mold.",
                    "Decorate with your favorite toppings.",
                    "Allow cake to thaw a few minutes before slicing to serve.",
                ]
            }
        
    },{
        "id":7,
        "name": "Apple Sorbet",
        "category" : 3,
        "image" : require('../src/img/no_banana_yonanas_recipes/apple_sorbet.jpg'),
        "recipes":
            {
                "prep" : 15,
                "cook" : 20,
                "serves" : 1
            }
        
    },{
        "id":8,
        "name": "Blueberry Sorbet",
        "category" : 3,
        "image" : require('../src/img/no_banana_yonanas_recipes/blueberry_sorbet.jpg'),
        "recipes":
            {
                "prep" : 15,
                "cook" : 20,
                "serves" : 1
            }
        
    },,{
        "id":9,
        "name": "Chili Spiced Mango Sorbet",
        "category" : 3,
        "image" : require('../src/img/no_banana_yonanas_recipes/chili_spiced_mango_sorbet.jpg'),
        "recipes":
            {
                "prep" : 15,
                "cook" : 20,
                "serves" : 1
            }
        
    }

]